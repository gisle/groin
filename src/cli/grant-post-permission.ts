import { RedisClient } from 'redis';
import { setupConf } from '../setup-conf';
import { ClientAuth } from '../client-auth';

const argv = require('minimist')(process.argv.slice(2));

if (!argv['id'] || argv.length > 1) {
    console.error('Usage: grant-post-permission.ts --name <client id>');
    process.exit(1);
}

try {
    const config = setupConf();
    config.required(['isbn:redis']);

    const redis = new RedisClient(config.get('isbn:redis'));

    const auth = new ClientAuth(config, redis);
    auth.init();

    auth.grantClientPostPermission(argv['id'])
    .then(() => console.log('Permission granted'))
    .then(() => process.exit(0))
    .catch(err => {
        console.error(err);
        process.exit(1);
    });
} catch (err) {
    console.error(err);
    process.exit(1);
}
