import { RedisClient } from 'redis';
import { setupConf } from '../setup-conf';
import { ClientAuth } from '../client-auth';

// Creates a test client with full privileges and outputs its token to stdout.
// Make sure your tests use the same JWT secret as this command was run with.

try {
    const config = setupConf();
    config.required(['isbn:redis']);

    const redis = new RedisClient(config.get('isbn:redis'));

    const auth = new ClientAuth(config, redis);

    auth.init();

    auth.createClient('test', 'test')
    .then(id => auth.grantClientPostPermission(id))
    .then(() => auth.getToken('test'))
    .then(console.log)
    .then(() => process.exit(0))
    .catch(err => {
        console.error(err);
        process.exit(1);
    });
} catch (err) {
    console.error(err);
    process.exit(1);
}
