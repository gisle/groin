import * as bodyParser from 'body-parser';
import * as express from 'express';
import { Express } from 'express';
import * as logger from 'morgan';
import { Provider } from 'nconf';
import * as redis from 'redis';
import * as passport from 'passport';
import * as nodemailer from 'nodemailer';

import { ISBNController } from './isbn-controller';
import { ClientAuth } from './client-auth';


function init(app: Express, config: Provider) {
    config.required(['isbn:host', 'isbn:port', 'isbn:redis']);

    // init express
    app.set('host', config.get('isbn:host'));
    app.set('port', config.get('isbn:port'));

    app.use(bodyParser.text({ type: 'text/csv' }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    if (process.env.NODE_ENV !== 'test') {
        app.use(logger('dev'));
    }

    // init redis
    const redisClient = redis.createClient(config.get('isbn:redis'));

    redisClient.on("error", error => {
        console.error(error);
    });

    // init email
    const emailConfig = config.get('isbn:email');
    let transporter = null;

    if (emailConfig && emailConfig.whenFewerThan && emailConfig.nodemailer) {
        transporter = nodemailer.createTransport(emailConfig.nodemailer);
        config.required(['isbn:email:from', 'isbn:email:to']);
        transporter.verify((err, success) => {
            if (err) {
                console.error('Error connecting to SMTP server:', err);
            } else {
                console.log('Successful connection to SMTP server');
            }
        })
    }

    // init auth
    const auth = new ClientAuth(config, redisClient);
    auth.init();
    app.use(passport.initialize());

    // init api
    app.use('/api/isbn/', passport.authenticate('jwt', {session: false}));
    app.use('/api/isbn', ISBNController(config, redisClient, auth, transporter));

    // error handling
    app.use((err, req, res, next) => {
        if (err) {
            if (process.env.NODE_ENV !== 'test') {
                console.error(err);
            }
            res.status(err.status || 500).json({error: { message: err.message, status: err.status }});
        } else {
            next();
        }
    });
}

export function start(config: Provider, callback) {
    const app = express();

    init(app, config);

    app.listen(
        config.get('isbn:port'),
        () => callback(null, app)
    );

    app.on('error', console.error);
}
