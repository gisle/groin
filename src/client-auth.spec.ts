import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import * as passport from 'passport';
import * as fs from 'fs';

import { ClientAuth } from './client-auth';

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Client auth', () => {
    /* istanbul ignore next */
    const mockRedis = {
        get: (key, cb) => cb(),
        set: (key, value, cb) => cb()
    }

    /* istanbul ignore next */
    const mockConfig = {
        get: () => '',
        required: () => {}
    }

    // passport part will be tested by integration tests only
    let sandbox: SinonSandbox;
    let auth: ClientAuth;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        auth = new ClientAuth(mockConfig as any, mockRedis as any);
    });

    afterEach(() => sandbox.restore());

    describe('init()', () => {
        beforeEach(() => {
            sandbox.stub(passport, 'use').returns(null);
        });

        it('should read jwtSecret from file if isbn:jwtSecretFile specified', () => {
            sandbox.stub(mockConfig, 'get').returns('foo');
            const spy = sandbox.stub(fs, 'readFileSync').returns(Buffer.from('foo'));

            auth.init();
            expect(spy.calledOnce).to.be.true;
        });

        it('should throw if jwtSecretFile specified but invalid', () => {
            sandbox.stub(mockConfig, 'get').returns('foo');
            const spy = sandbox.stub(fs, 'readFileSync').throws(Error());

            expect(() => auth.init()).to.throw();
        });

        it('should throw if jwtSecret is not specified as either file or string value', () => {
            sandbox.stub(mockConfig, 'get')
                .withArgs('isbn:jwtSecretFile').returns(undefined)
                .withArgs('isbn:jwtSecret').returns(undefined);

            expect(() => auth.init()).to.throw();
        });
    });

    describe('createClient()', () => {
        it('should add a client', async () => {
            const spy = sandbox.spy(auth as any, 'set');

            await auth.createClient('foo');

            expect(spy.calledOnce).to.be.true;
            expect(JSON.parse(spy.args[0][1])).to.have.property('name', 'foo');
        });

        it('should throw if redis SET fails', async () => {
            sandbox.stub(auth as any, 'set').rejects(Error('foo'));

            await expect(auth.createClient('foo')).to.be.rejectedWith('Database error');
        });

        it('should allow creating a client with a predefined ID', async () => {
            const spy = sandbox.spy(auth as any, 'set');

            const id = await auth.createClient('foo', 'bar');
            expect(id).to.equal('bar');

            expect(spy.calledOnce).to.be.true;
            expect(JSON.parse(spy.args[0][1])).to.have.property('name', 'foo');
            expect(JSON.parse(spy.args[0][1])).to.have.property('id', 'bar');
        });
    });

    describe('clientCanAddNumbers()', () => {
        it('should return rights stored for the given client', async () => {
            let canAddNumbers = auth.clientCanAddNumbers({ user: { canAddNumbers: false } } as any);

            expect(canAddNumbers).to.be.false;

            canAddNumbers = auth.clientCanAddNumbers({ user: { canAddNumbers: true } } as any);

            expect(canAddNumbers).to.be.true;

            canAddNumbers = auth.clientCanAddNumbers({ user: {} } as any);

            expect(canAddNumbers).to.be.false;
        });
    });

    describe('grantClientPostPermission()', () => {
        it('should change canAddNumbers to true for a given client', async () => {
            sandbox.stub(auth as any, 'get').resolves(JSON.stringify({name: 'foo', canAddNumbers: false, id: '123'}));
            const spy = sandbox.stub(auth as any, 'set');

            await auth.grantClientPostPermission('foo');

            expect(spy.calledOnce).to.be.true;
            expect(JSON.parse(spy.args[0][1])).to.have.property('canAddNumbers', true);
        });

        it('should throw if client does not exist', async () => {
            sandbox.stub(auth as any, 'get').resolves(null);

            await expect(auth.grantClientPostPermission('foo')).to.be.rejectedWith('Client foo not found');
        });

        it('should throw if redis GET fails', async () => {
            sandbox.stub(auth as any, 'get').rejects('foo');

            await expect(auth.grantClientPostPermission('foo')).to.be.rejectedWith('Database error');
        });

        it('should throw if redis SET fails', async () => {
            sandbox.stub(auth as any, 'get').resolves(JSON.stringify({name: 'foo', canAddNumbers: false, id: '123'}));
            sandbox.stub(auth as any, 'set').rejects('foo');

            await expect(auth.grantClientPostPermission('foo')).to.be.rejectedWith('Database error');
        });
    });

    describe('revokeClientPostPermission()', () => {
        it('should change canAddNumbers to true for a given client', async () => {
            sandbox.stub(auth as any, 'get').resolves(JSON.stringify({name: 'foo', canAddNumbers: true, id: '123'}));
            const spy = sandbox.stub(auth as any, 'set');

            await auth.revokeClientPostPermission('foo');

            expect(spy.calledOnce).to.be.true;
            expect(JSON.parse(spy.args[0][1])).to.have.property('canAddNumbers', false);
        });

        it('should throw if client does not exist', async () => {
            sandbox.stub(auth as any, 'get').resolves(null);

            await expect(auth.revokeClientPostPermission('foo')).to.be.rejectedWith('Client foo not found');
        });

        it('should throw if redis GET fails', async () => {
            sandbox.stub(auth as any, 'get').rejects('foo');

            await expect(auth.revokeClientPostPermission('foo')).to.be.rejectedWith('Database error');
        });

        it('should throw if redis SET fails', async () => {
            sandbox.stub(auth as any, 'get').resolves(JSON.stringify({name: 'foo', canAddNumbers: false, id: '123'}));
            sandbox.stub(auth as any, 'set').rejects('foo');

            await expect(auth.revokeClientPostPermission('foo')).to.be.rejectedWith('Database error');
        });
    });
});
