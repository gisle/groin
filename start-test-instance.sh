# Use this as entry point if you are using the application as a docker service in integration tests.

# Create a test user with full privileges.
# The token created only depends on the JWT secret,
# so a user should be able to either look at the output of this script or
# call the same script with the same setup and get the token.
./node_modules/.bin/ts-node src/cli/create-test-token.ts

npm start
