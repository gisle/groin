import * as chai from 'chai';
import * as request from 'supertest';
import { RedisClient } from 'redis';

import { spawn } from 'child_process';
import * as concat from 'concat-stream';

import { setupConf } from './src/setup-conf';
import { ClientAuth } from './src/client-auth';

import { start } from './src/server';

const expect = chai.expect;

const config = setupConf();

const redis = new RedisClient(config.get('isbn:redis'));

describe('ISBN server', () => {
	let server;
	let token;
    let auth: ClientAuth;
    let clientId;

	before(done => {
		start(config, (err, app) => {
			server = app;
			done(err);
		});
	});

	beforeEach(done => redis.flushall(done));

	beforeEach(async () => {
		auth = new ClientAuth(config, redis);
		// auth.init(server); // already done in start()
		clientId = await auth.createClient('test-client');
        auth.init();
		token = auth.getToken(clientId);
	});

	describe('API', () => {
    	it('/api/isbn/ should refuse unauthenticated requests', async () => {
            await request(server).get('/api/isbn')
            .expect(401);

            await request(server).put('/api/isbn')
            .expect(401);

            // phony token
            await request(server).get('/api/isbn')
            .set('Authorization', 'Bearer 123123.456456.789789')
            .expect(401);
        });

        it('/api/isbn/ should refuse requests for non-existent clients with valid tokens', async () => {
            const token = auth.getToken('foo');
            await request(server).get('/api/isbn')
            .set('Authorization', 'Bearer ' + token)
            .expect(401);
        });

        describe('Read/Write', () => {
            beforeEach(async () => {
                await auth.grantClientPostPermission(clientId);
            });

    	    it('GET /api/isbn/ should accept authenticated GET request and return empty list if none posted', async () => {
    	    	await request(server).get('/api/isbn/')
    	    	.set('Authorization', 'Bearer ' + token)
    	    	.expect(200)
    	    	.expect(res => expect(res.body).to.deep.equal([]));
    	    });

            it('PUT /api/isbn/ should deny clients who are not allowed to add new numbers', async () => {
                await auth.revokeClientPostPermission(clientId);

                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(401);
            });

    	    it('PUT /api/isbn/ should accept json array of ISBN numbers', async () => {
                const list = [ '978-0-596-52068-7', '0-596-52068-9' ];

                await request(server).put('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .send(list)
                .expect(200)
                .expect(res => expect(res.body).to.have.property('message', 'accepted'));

                await request(server).get('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => {
                    expect(res.body.sort()).to.deep.equal(list.sort().map(number => number.replace(/-/g, '')));
                });
            });

            it('PUT /api/isbn/ should accept csv file of ISBN numbers', async () => {
                const list = [ '978-0-596-52068-7', '0-596-52068-9' ];
                const csv = list.join('\n') + '\n'; // add empty line, should be ignored

                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send(csv)
                .set('Content-Type', 'text/csv')
                .expect(200)
                .expect(res => expect(res.body).to.have.property('message', 'accepted'));

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => {
                    expect(res.body.sort()).to.deep.equal(list.sort().map(number => number.replace(/-/g, '')));
                });
            });

            it('PUT /api/isbn/ should accept empty list of ISBN numbers', async () => {
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([])
                .expect(200)
                .expect(res => expect(res.body).to.have.property('message', 'accepted'));

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => {
                    expect(res.body).to.deep.equal([]);
                });
            });

            it('PUT /api/isbn/ should ignore numbers that are added twice', async () => {
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([ '978-0-596-52068-7' ])
                .expect(200);

                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([ '978-0-596-52068-7' ])
                .expect(200);

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.have.length(1));
            });

            it('PUT /api/isbn/ should ignore duplicate numbers in same list', async () => {
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([ '978-0-596-52068-7', '978-0-596-52068-7' ])
                .expect(200);

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.have.length(1));
            });
        });

        describe('Errors', () => {
            beforeEach(async () => {
                await auth.grantClientPostPermission(clientId);
            });

            it('PUT /api/isbn/ should not accept non-array request bodies', async () => {
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send({})
                .expect(400);
            });

            it('PUT /api/isbn/ should not accept non-string array elements', async () => {
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([{}])
                .expect(400);
            });

            it('PUT /api/isbn/ should not accept random strings as ISBN numbers', async () => {
                await request(server).put('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .send([ '123' ])
                .expect(400);

                await request(server).get('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([]));
            });

            it('PUT /api/isbn/ should not accept ISBN-13 numbers with invalid checksum', async () => {
                await request(server).put('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .send([ '978-0-596-52068-6' ])
                .expect(400);

                await request(server).get('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([]));
            });

            it('PUT /api/isbn/ should not accept ISBN-10 numbers with invalid checksum', async () => {
                await request(server).put('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .send([ '0-596-52068-8' ])
                .expect(400);

                await request(server).get('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([]));
            });

            it('PUT /api/isbn/ should not save any numbers in case one ISBN is invalid', async () => {
                await request(server).put('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .send([ '978-0-596-52068-7', '123' ])
                .expect(400);

                await request(server).get('/api/isbn')
    	    	.set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([]));
            });

            it('PUT /api/isbn/ should not save any numbers if adding one or more already used numbers', async () => {
                const list = [ '9780596520687', '0596520689', '097522980X' ];
                let usedNumber: string;

                // first add two numbers
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send([list[0], list[1]])
                .expect(200);

                // check that numbers added
                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect('X-Total-Count', '2');

                await request(server).post('/api/isbn/reserve')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .then(res => usedNumber = res.body.isbn);

                // check that one number used
                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect('X-Total-Count', '1');

                // put list that includes the numer that was used, an unused number and a new number
                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send(list)
                .expect(409)
                .expect(res => expect(res.body.error).to.have.property('message').that.includes(usedNumber));

                // no new numbers should have been added
                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect('X-Total-Count', '1');
            });
        });

        describe('Reserve/Release', () => {
            it('POST /api/isbn/(reserve|release/:isbn) should reserve and release an ISBN number', async () => {
                const list = [ '978-0-596-52068-7' ];
                const stripped = '9780596520687';

                await auth.grantClientPostPermission(clientId);

                await request(server).put('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .send(list)
                .expect(200);

                await request(server).post('/api/isbn/reserve')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.have.property('isbn', stripped));

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([]));

                // verifying that unnormalized isbns work as they should
                await request(server).post('/api/isbn/release/' + list[0])
                .set('Authorization', 'Bearer ' + token)
                .expect(200);

                await request(server).get('/api/isbn')
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal([stripped]));
            });

            it('POST /api/isbn/release/:isbn should return 404 if number unknown', async () => {
                await request(server).post('/api/isbn/release/123')
                .set('Authorization', 'Bearer ' + token)
                .expect(404);
            });

            it('POST /api/isbn/reserve should return 404 if list empty', async () => {
                await request(server).post('/api/isbn/reserve')
                .set('Authorization', 'Bearer ' + token)
                .expect(404);
            });
        });
	});

    describe('CLI', function() {
        this.timeout(10000); // These tests might take a bit longer, especially in CI environment

        // Test setup from
        // https://medium.com/@zorrodg/integration-tests-on-node-js-cli-part-1-why-and-how-fa5b1ba552fe

        function createProcess(processPath, args = [], env = null) {
            args = [processPath].concat(args);

            return spawn('node', args, { env: Object.assign({ NODE_ENV: 'test' }, env) });
        }

        function execute(processPath, args = []): Promise<string> {
            const childProcess = createProcess(processPath, args, process.env);
            //childProcess.stdin.setEncoding('utf-8');
            const promise = new Promise<string>((resolve, reject) => {
                childProcess.stderr.once('data', err => {
                    reject(err.toString());
                });
                childProcess.on('error', reject);
                childProcess.stdout.pipe(
                    concat(result => {
                        resolve(result.toString());
                    })
                );
            });
            return promise;
        }

        describe('create-client', () => {
            it('should create client and return id', async () => {
                const response = await execute('./src/cli/create-client.ts', ['--name', 'foo']);
                expect(response).to.match(/Client created with id: [0-9a-f\-]*/);

                const id = response.trim().replace(/.*: /, '');
                expect(await auth.getClient(id)).to.have.property('name', 'foo');
            });

            it('should fail if no --name argument', async () => {
                try {
                    await execute('./src/cli/create-client.ts');
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.match(/Usage:/);
                }
            });

            it('should fail if client name null', async () => {
                try {
                    await execute('./src/cli/create-client.ts', ['--name']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });
        });

        describe('create-token', () => {
            it('should generate token', async () => {
                const response = await execute('./src/cli/create-token.ts', ['--id', clientId]);
                expect(response).to.match(/\w+\.\w+\.\w+/);
            });

            it('should fail if no --id argument', async () => {
                try {
                    await execute('./src/cli/create-token.ts');
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.match(/Usage:/);
                }
            });

            it('should fail if --id undefined', async () => {
                try {
                    await execute('./src/cli/create-token.ts', ['--id']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });

            it('should fail if --id does not exist', async () => {
                try {
                    await execute('./src/cli/create-token.ts', ['--id', 'bar']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });
        });

        describe('grant-post-permission', () => {
            it('should grant client post permission', async () => {
                await expect(await auth.getClient(clientId)).to.have.property('canAddNumbers', false);
                await execute('./src/cli/grant-post-permission.ts', ['--id', clientId]);
                await expect(await auth.getClient(clientId)).to.have.property('canAddNumbers', true);
            });

            it('should fail if no --id argument', async () => {
                try {
                    await execute('./src/cli/grant-post-permission.ts');
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.match(/Usage:/);
                }
            });

            it('should fail if --id undefined', async () => {
                try {
                    await execute('./src/cli/grant-post-permission.ts', ['--id']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });

            it('should fail if --id does not exist', async () => {
                try {
                    await execute('./src/cli/grant-post-permission.ts', ['--id', 'bar']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });
        });

        describe('revoke-post-permission', () => {
            it('should grant client post permission', async () => {
                await expect(await auth.getClient(clientId)).to.have.property('canAddNumbers', false);
                await execute('./src/cli/grant-post-permission.ts', ['--id', clientId]);
                await expect(await auth.getClient(clientId)).to.have.property('canAddNumbers', true);
                await execute('./src/cli/revoke-post-permission.ts', ['--id', clientId]);
                await expect(await auth.getClient(clientId)).to.have.property('canAddNumbers', false);
            });

            it('should fail if no --id argument', async () => {
                try {
                    await execute('./src/cli/revoke-post-permission.ts');
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.match(/Usage:/);
                }
            });

            it('should fail if --id undefined', async () => {
                try {
                    await execute('./src/cli/revoke-post-permission.ts', ['--id']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });

            it('should fail if --id does not exist', async () => {
                try {
                    await execute('./src/cli/revoke-post-permission.ts', ['--id', 'bar']);
                    expect(true).to.be.false;
                } catch (err) {
                    expect(err).to.not.be.null;
                }
            });
        });
    });
});
